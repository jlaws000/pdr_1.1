﻿using System.Text.RegularExpressions;

namespace PDR.PatientBooking.Service.Utils
{
    public static class ValidatorHelper
    {
        public static bool ValidateEmailAddress(string email)
        {
            var regex = new Regex(@"^[^@\s]+@[^@\s]+\.[^@\s]+$", RegexOptions.IgnoreCase);
            return !regex.IsMatch(email);
        }
    }
}
