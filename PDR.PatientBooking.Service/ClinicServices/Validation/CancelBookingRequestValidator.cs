﻿using System.Linq;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Service.BookingServices.Requests;
using PDR.PatientBooking.Service.Validation;

namespace PDR.PatientBooking.Service.ClinicServices.Validation
{
    public class CancelBookingRequestValidator : ICancelBookingRequestValidator
    {
        private readonly PatientBookingContext _context;

        public CancelBookingRequestValidator(PatientBookingContext context)
        {
            _context = context;
        }

        public PdrValidationResult ValidateRequest(CancelBookingRequest request)
        {
            var result = new PdrValidationResult(true);

            if (BookingNotFound(request, ref result))
                return result;

            return result;
        }

        private bool BookingNotFound(CancelBookingRequest request, ref PdrValidationResult result)
        {
            if (!_context.Order.Any(x =>
                x.PatientId == request.PatientId &&
                x.StartTime == request.StartTime &&
                x.DoctorId == request.DoctorId &&
                x.IsCancelled == false))
            {
                result.PassedValidation = false;
                result.Errors.Add("This booking could not be found.");
                return true;
            }

            return false;
        }

    }
}
