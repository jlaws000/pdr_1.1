﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Service.BookingServices.Requests;
using PDR.PatientBooking.Service.Validation;

namespace PDR.PatientBooking.Service.BookingServices.Validation
{
    public class AddBookingRequestValidator : IAddBookingRequestValidator
    {
        private readonly PatientBookingContext _context;

        public AddBookingRequestValidator(PatientBookingContext context)
        {
            _context = context;
        }
        public PdrValidationResult ValidateRequest(AddBookingRequest request)
        {
            var result = new PdrValidationResult(true);

            if (AppointmentIsForAPastDate(request, ref result))
                return result;

            if (RequestedDoctorisBusy(request, ref result))
                return result;

            return result;
        }

        private bool RequestedDoctorisBusy(AddBookingRequest request, ref PdrValidationResult result)
        {
            var errors = new List<string>();

            var booking = _context.Order.FirstOrDefault(x =>
                x.Doctor.Id == request.DoctorId &&
                x.StartTime == request.StartTime
            );

            if (booking != null)
            {
                errors.Add("The requested doctor is busy");
            }

            if (errors.Any())
            {
                result.PassedValidation = false;
                result.Errors.AddRange(errors);
                return true;
            }

            return false;
        }

        private bool AppointmentIsForAPastDate(AddBookingRequest request, ref PdrValidationResult result)
        {
            var errors = new List<string>();

            if (request.StartTime < DateTime.Now)
                errors.Add("An appointment for the past can't be made");

            if (errors.Any())
            {
                result.PassedValidation = false;
                result.Errors.AddRange(errors);
                return true;
            }

            return false;
        }
    }
}
