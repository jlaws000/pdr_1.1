﻿using System;
using System.Linq;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Data.Models;
using PDR.PatientBooking.Service.BookingServices.Requests;
using PDR.PatientBooking.Service.BookingServices.Responses;
using PDR.PatientBooking.Service.BookingServices.Validation;
using PDR.PatientBooking.Service.ClinicServices.Validation;

namespace PDR.PatientBooking.Service.BookingServices
{
    public class BookingService : IBookingService
    {
        private readonly PatientBookingContext _context;
        private readonly IAddBookingRequestValidator _addBookingValidator;
        private readonly ICancelBookingRequestValidator _cancelBookingValidator;

        public BookingService(PatientBookingContext context, IAddBookingRequestValidator addBookingValidator, ICancelBookingRequestValidator cancelBookingValidator)
        {
            _context = context;
            _addBookingValidator = addBookingValidator;
            _cancelBookingValidator = cancelBookingValidator;
        }

        public void AddBooking(AddBookingRequest request)
        {
            var validationResult = _addBookingValidator.ValidateRequest(request);

            if (!validationResult.PassedValidation)
            {
                throw new ArgumentException(validationResult.Errors.First());
            }

            _context.Order.Add(new Order
            {
                Id = new Guid(),
                StartTime = request.StartTime,
                EndTime = request.EndTime,
                PatientId = request.PatientId,
                DoctorId = request.DoctorId,
                SurgeryType = (int)_context.Patient.FirstOrDefault(x => x.Id == request.PatientId).Clinic.SurgeryType,
                Patient = _context.Patient.FirstOrDefault(x => x.Id == request.PatientId),
                Doctor = _context.Doctor.FirstOrDefault(x => x.Id == request.DoctorId)
            });

            _context.SaveChanges();
        }

        public GetNextAppointmentResponse GetPatientsNextAppointment(long patientId)
        {
            var bookings = _context.Order.OrderBy(x => x.StartTime).ToList();

            if (bookings.Where(x => x.Patient.Id == patientId && x.IsCancelled == false).Count() == 0)
            {
                return null;
            }
            else
            {
                var bookings2 = bookings.Where(x => x.PatientId == patientId && x.IsCancelled == false);
                if (bookings2.Where(x => x.StartTime > DateTime.Now).Count() == 0)
                {
                    return null;
                }
                else
                {
                    var bookings3 = bookings2.Where(x => x.StartTime > DateTime.Now);
                    return new GetNextAppointmentResponse
                    {
                        NextAppointment = new GetNextAppointmentResponse.Order
                        {
                            Id = bookings3.First().Id,
                            DoctorId = bookings3.First().DoctorId,
                            StartTime = bookings3.First().StartTime,
                            EndTime = bookings3.First().EndTime
                        }
                    };
                }
            }
        }

        public void CancelOrder(CancelBookingRequest request)
        {
            var validationResult = _cancelBookingValidator.ValidateRequest(request);

            if (!validationResult.PassedValidation)
            {
                throw new ArgumentException(validationResult.Errors.First());
            }

            var booking = _context.Order.FirstOrDefault(x =>
                x.PatientId == request.PatientId &&
                x.StartTime == request.StartTime &&
                x.DoctorId == request.DoctorId);

            // it won't be null because the validation will have caught it above if so
            booking.IsCancelled = true;

            _context.Order.Update(booking);

            _context.SaveChanges();
        }
    }
}