﻿using System;
using AutoFixture;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Data.Models;
using PDR.PatientBooking.Service.BookingServices;
using PDR.PatientBooking.Service.BookingServices.Requests;
using PDR.PatientBooking.Service.BookingServices.Responses;
using PDR.PatientBooking.Service.BookingServices.Validation;
using PDR.PatientBooking.Service.ClinicServices.Validation;
using PDR.PatientBooking.Service.Validation;

namespace PDR.PatientBooking.Service.Tests.BookingServices
{
    [TestFixture]
    public class BookingServiceTests
    {
        private MockRepository _mockRepository;
        private IFixture _fixture;

        private PatientBookingContext _context;

        private BookingService _bookingService;
        private Mock<IAddBookingRequestValidator> _addBookingValidator;
        private Mock<ICancelBookingRequestValidator> _cancelBookingvalidator;

        [SetUp]
        public void SetUp()
        {
            // Boilerplate
            _mockRepository = new MockRepository(MockBehavior.Strict);
            _fixture = new Fixture();

            //Prevent fixture from generating circular references
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior(1));

            // Mock setup
            _context = new PatientBookingContext(new DbContextOptionsBuilder<PatientBookingContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options);
            _addBookingValidator = _mockRepository.Create<IAddBookingRequestValidator>();
            _cancelBookingvalidator = _mockRepository.Create<ICancelBookingRequestValidator>();

            // Mock default
            SetupMockDefaults();

            // Sut instantiation
            _bookingService = new BookingService(
                _context,
                _addBookingValidator.Object,
                _cancelBookingvalidator.Object
            );
        }

        [Test]
        public void AddBooking_ValidatesRequest()
        {
            //arrange
            var request = _fixture.Create<AddBookingRequest>();
            request.StartTime = DateTime.Now.AddDays(1);

            _context.Patient.Add(new Patient
            {
                Id = request.PatientId,
                Clinic = new Clinic
                {
                    SurgeryType = (SurgeryType)request.SurgeryType
                }
            });
            _context.Doctor.Add(new Doctor
            {
                Id = request.DoctorId
            });
            _context.SaveChanges();

            //act
            _bookingService.AddBooking(request);

            //assert
            _addBookingValidator.Verify(x => x.ValidateRequest(request), Times.Once);
        }

        [Test]
        public void AddBooking_AddsBookingToContextWithGeneratedId()
        {
            //arrange

            var request = _fixture.Create<AddBookingRequest>();
            _context.Patient.Add(new Patient
            {
                Id = request.PatientId,
                Clinic = new Clinic
                {
                    SurgeryType = (SurgeryType)request.SurgeryType
                }
            });
            _context.Doctor.Add(new Doctor
            {
                Id = request.DoctorId
            });
            _context.SaveChanges();

            var expected = new Order
            {
                StartTime = request.StartTime,
                EndTime = request.EndTime,
                SurgeryType = request.SurgeryType,
                PatientId = request.PatientId,
                DoctorId = request.DoctorId,
                IsCancelled = false
            };

            //act
            _bookingService.AddBooking(request);

            //assert
            _context.Order.Should().ContainEquivalentOf(expected, options => options
                .Excluding(booking => booking.Id)
                .Excluding(booking => booking.Doctor)
                .Excluding(booking => booking.Patient));
        }

        [Test]
        public void BookingService_GetsPatientsAppointment()
        {
            var order = CreateOrder(1, false);
            _context.Order.Add(order);
            _context.SaveChanges();

            var expected = new GetNextAppointmentResponse
            {
                NextAppointment = new GetNextAppointmentResponse.Order
                {
                    Id = order.Id,
                    StartTime = order.StartTime,
                    EndTime = order.EndTime,
                    DoctorId = order.DoctorId,
                }
            };

            //act
            var res = _bookingService.GetPatientsNextAppointment(order.PatientId);

            //assert
            res.Should().BeEquivalentTo(expected);
        }


        [Test]
        public void BookingService_GetsPatientsNextAppointment_ThatIsntCancelled_ReturnsNullIfNotFound()
        {
            var cancelledOrder = CreateOrder(1, true);
            _context.Order.Add(cancelledOrder);
            _context.SaveChanges();

            //act
            var res = _bookingService.GetPatientsNextAppointment(cancelledOrder.PatientId);

            //assert
            res.Should().BeNull();
        }


        [Test]
        public void BookingService_ReturnsNullIfNoAppointmentsMatchPatientsId()
        {
            var order = _fixture.Create<Order>();
            _context.Order.Add(order);
            _context.SaveChanges();

            //act
            var res = _bookingService.GetPatientsNextAppointment(12345);

            //assert
            res.Should().BeNull();
        }

        [Test]
        public void BookingService_ReturnsNullIfPatientIdMatchesButNoFutureStartTime()
        {
            var order = new Order
            {
                DoctorId = 1,
                PatientId = 2,
                Doctor = new Doctor { Id = 1 },
                Patient = new Patient { Id = 2 },
                SurgeryType = 1,
                StartTime = DateTime.Now.AddDays(-1),
                EndTime = DateTime.Now.AddDays(-1).AddHours(1)
            };
            _context.Order.Add(order);
            _context.SaveChanges();

            //act
            var res = _bookingService.GetPatientsNextAppointment(order.PatientId);

            //assert
            res.Should().BeNull();
        }

        [Test]
        public void CancelBooking_SetsIsCancelledIfFound()
        {
            // arrange
            var order = CreateOrder(1, false);
            _context.Order.Add(order);
            _context.SaveChanges();

            var cancelOrderRequest = new CancelBookingRequest
            {
                DoctorId = order.DoctorId,
                PatientId = order.PatientId,
                StartTime = order.StartTime
            };

            var expected = new Order
            {
                StartTime = cancelOrderRequest.StartTime,
                PatientId = cancelOrderRequest.PatientId,
                DoctorId = cancelOrderRequest.DoctorId,
                IsCancelled = true,
                SurgeryType = order.SurgeryType,
                EndTime = order.EndTime
            };

            //act
            _bookingService.CancelOrder(cancelOrderRequest);

            _context.Order.Should().ContainEquivalentOf(expected, options => options
                .Excluding(booking => booking.Id)
                .Excluding(booking => booking.Doctor)
                .Excluding(booking => booking.Patient));
        }

        private Order CreateOrder(int addDays, bool isCancelled)
        {
            var order = new Order
            {
                DoctorId = 1,
                PatientId = 2,
                Doctor = new Doctor { Id = 1 },
                Patient = new Patient { Id = 2 },
                SurgeryType = 1,
                StartTime = DateTime.Now.AddDays(addDays),
                EndTime = DateTime.Now.AddDays(addDays).AddHours(1),
                IsCancelled = isCancelled
            };
            return order;
        }

        private void SetupMockDefaults()
        {
            _addBookingValidator.Setup(x => x.ValidateRequest(It.IsAny<AddBookingRequest>()))
                .Returns(new PdrValidationResult(true));

            _cancelBookingvalidator.Setup(x => x.ValidateRequest(It.IsAny<CancelBookingRequest>()))
                .Returns(new PdrValidationResult(true));
        }
    }
}
