﻿using System;
using AutoFixture;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Data.Models;
using PDR.PatientBooking.Service.BookingServices.Requests;
using PDR.PatientBooking.Service.BookingServices.Validation;

namespace PDR.PatientBooking.Service.Tests.BookingServices.Validation
{
    [TestFixture]
    public class AddBookingValidatorTests
    {
        private IFixture _fixture;

        private PatientBookingContext _context;

        private AddBookingRequestValidator _addBookingRequestValidator;

        [SetUp]
        public void SetUp()
        {
            // Boilerplate
            _fixture = new Fixture();

            //Prevent fixture from generating from entity circular references 
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior(1));

            // Mock setup
            _context = new PatientBookingContext(new DbContextOptionsBuilder<PatientBookingContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options);

            // Mock default
            SetupMockDefaults();

            // Sut instantiation
            _addBookingRequestValidator = new AddBookingRequestValidator(
                _context
            );
        }

        private void SetupMockDefaults()
        {

        }

        [Test]
        public void ValidateRequest_AllChecksPass_ReturnsPassedValidationResult()
        {
            //arrange
            var request = GetValidRequest();

            //act
            var res = _addBookingRequestValidator.ValidateRequest(request);

            //assert
            res.PassedValidation.Should().BeTrue();
        }

        [Test]
        public void ValidateRequest_AppointmentInThePast__ReturnsFailedValidationResult()
        {
            //arrange
            var request = GetValidRequest();
            request.StartTime = DateTime.Now.AddDays(-1);

            //act
            var res = _addBookingRequestValidator.ValidateRequest(request);

            //assert
            res.PassedValidation.Should().BeFalse();
            res.Errors.Should().Contain("An appointment for the past can't be made");
        }

        [Test]
        public void ValidateRequest_RequestedDoctorisBusyAtThatTime__ReturnsFailedValidationResult()
        {
            //arrange
            var orderDoctorisBusyWith = GetOrderWithDoctor();
            _context.Order.Add(orderDoctorisBusyWith);
            _context.SaveChanges();

            var request = GetValidRequest();
            request.StartTime = orderDoctorisBusyWith.StartTime;
            request.DoctorId = orderDoctorisBusyWith.DoctorId;

            //act
            var res = _addBookingRequestValidator.ValidateRequest(request);

            //assert
            res.PassedValidation.Should().BeFalse();
            res.Errors.Should().Contain("The requested doctor is busy");
        }

        [Test]
        public void ValidateRequest_RequestedDoctorisNotBusyAtThatTime__ReturnsPassedValidationResult()
        {
            //arrange
            var orderForADifferentTime = GetOrderWithDoctor();
            _context.Order.Add(orderForADifferentTime);
            _context.SaveChanges();

            var request = GetValidRequest();
            request.StartTime = orderForADifferentTime.StartTime.AddHours(1);
            request.DoctorId = orderForADifferentTime.DoctorId;

            //act
            var res = _addBookingRequestValidator.ValidateRequest(request);

            //assert
            res.PassedValidation.Should().BeTrue();
        }

        [Test]
        public void ValidateRequest_DifferentDoctorisBusyAtThatTime__ReturnsPassedValidationResult()
        {
            //arrange
            var orderForADiffDoctor = GetOrderWithDoctor();
            _context.Order.Add(orderForADiffDoctor);
            _context.SaveChanges();

            var request = GetValidRequest();
            request.StartTime = orderForADiffDoctor.StartTime;
            request.DoctorId = 2;

            //act
            var res = _addBookingRequestValidator.ValidateRequest(request);

            //assert
            res.PassedValidation.Should().BeTrue();
        }

        private Order GetOrderWithDoctor()
        {
            var request = _fixture.Build<Order>()
                .With(x => x.StartTime, DateTime.Today.AddDays(1).AddHours(9))
                .With(x => x.EndTime, DateTime.Today.AddDays(1).AddHours(10))
                .With(x => x.SurgeryType, 1)
                .With(x => x.Patient, new Patient { Id = 17 })
                .With(x => x.PatientId, 17)
                .With(x => x.DoctorId, 1)
                .With(x => x.Doctor, new Doctor { Id = 1 })
                .Create();
            return request;
        }

        private AddBookingRequest GetValidRequest()
        {
            var request = _fixture.Create<AddBookingRequest>();
            request.StartTime = DateTime.Now.AddDays(1);
            return request;
        }
    }
}
