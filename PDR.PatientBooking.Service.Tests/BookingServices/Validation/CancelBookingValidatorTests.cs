﻿using System;
using AutoFixture;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Data.Models;
using PDR.PatientBooking.Service.BookingServices.Requests;
using PDR.PatientBooking.Service.BookingServices.Validation;
using PDR.PatientBooking.Service.ClinicServices.Validation;

namespace PDR.PatientBooking.Service.Tests.BookingServices.Validation
{
    [TestFixture()]
    public class CancelBookingValidatorTests
    {
        private IFixture _fixture;

        private PatientBookingContext _context;

        private CancelBookingRequestValidator _cancelBookingRequestValidator;

        [SetUp]
        public void SetUp()
        {
            // Boilerplate
            _fixture = new Fixture();

            //Prevent fixture from generating from entity circular references 
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior(1));

            // Mock setup
            _context = new PatientBookingContext(new DbContextOptionsBuilder<PatientBookingContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options);

            // Mock default
            SetupMockDefaults();

            // Sut instantiation
            _cancelBookingRequestValidator = new CancelBookingRequestValidator(
                _context
            );
        }

        private void SetupMockDefaults()
        {

        }

        [Test]
        public void ValidateRequest_AppointmentFoundForThatPatientAndTime__ReturnsPassedValidationResult()
        {
            //arrange
            var orderToCancel = GetOrderToCancel();
            _context.Order.Add(orderToCancel);
            _context.SaveChanges();

            var request = new CancelBookingRequest
            {
                StartTime = orderToCancel.StartTime,
                PatientId = orderToCancel.PatientId,
                DoctorId = orderToCancel.DoctorId
            };

            //act
            var res = _cancelBookingRequestValidator.ValidateRequest(request);

            //assert
            res.PassedValidation.Should().BeTrue();
        }

        [TestCase(17, 2)]
        [TestCase(999, 1)]
        public void ValidateRequest_OrderCouldNotBeFound__ReturnsFailedValidationResult(long patientId, long doctorId)
        {
            //arrange
            var orderToCancel = GetOrderToCancel();
            _context.Order.Add(orderToCancel);
            _context.SaveChanges();

            var request = new CancelBookingRequest
            {
                StartTime = orderToCancel.StartTime,
                PatientId = patientId,
                DoctorId = doctorId
            };

            //act
            var res = _cancelBookingRequestValidator.ValidateRequest(request);

            //assert
            res.PassedValidation.Should().BeFalse();
            res.Errors.Should().Contain("This booking could not be found.");
        }

        [Test]
        public void ValidateRequest_OrderAlreadyCancelled__ReturnsFailedValidationResult()
        {
            //arrange
            var cancelledOrder = GetOrderToCancel();
            cancelledOrder.IsCancelled = true;
            _context.Order.Add(cancelledOrder);
            _context.SaveChanges();

            var request = new CancelBookingRequest
            {
                StartTime = cancelledOrder.StartTime,
                PatientId = cancelledOrder.PatientId,
                DoctorId = cancelledOrder.DoctorId
            };

            //act
            var res = _cancelBookingRequestValidator.ValidateRequest(request);

            //assert
            res.PassedValidation.Should().BeFalse();
            res.Errors.Should().Contain("This booking could not be found.");
        }

        private Order GetOrderToCancel()
        {
            var request = _fixture.Build<Order>()
                .With(x => x.StartTime, DateTime.Today.AddDays(1).AddHours(9))
                .With(x => x.EndTime, DateTime.Today.AddDays(1).AddHours(10))
                .With(x => x.SurgeryType, 1)
                .With(x => x.Patient, new Patient { Id = 17 })
                .With(x => x.PatientId, 17)
                .With(x => x.DoctorId, 1)
                .With(x => x.Doctor, new Doctor { Id = 1 })
                .With(x => x.IsCancelled, false)
                .Create();
            return request;
        }
    }
}
